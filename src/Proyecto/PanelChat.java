package Proyecto;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class PanelChat extends javax.swing.JFrame {
    private Socket cliente;
    private final int PUERTOC = 1000;//Puesto por el cual se conecta y salen los mensajes
    private String host = "localhost";
    private DataOutputStream salida;
    private String nombre;
    /**
     * Creates new form PanelChat2
     */
    public PanelChat() {
        initComponents();
        try{
            nombre = JOptionPane.showInputDialog("Su Nombre");
            super.setTitle(super.getTitle() + nombre);
            bienvenida.setText("Bienvenid@ "+nombre);
            super.setVisible(true);
            cliente = new Socket(host, PUERTOC);//permite la conexion al servidor
            DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
            salida.writeUTF(nombre);
            ChatClient hilo = new ChatClient(cliente, this);
            hilo.start();
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.toString());
            
        }
    }
    
    public void mensajeria(String msg) { 
        this.chat.append(" " + msg + "\n"); //Mostrar mensajes
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelNorte = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        bienvenida = new javax.swing.JLabel();
        panelSur = new javax.swing.JPanel();
        msj = new javax.swing.JTextField();
        enviar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        chat = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaConectados = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(51, 51, 255));
        setForeground(new java.awt.Color(51, 51, 255));

        panelNorte.setBackground(new java.awt.Color(51, 51, 255));
        panelNorte.setLayout(new java.awt.BorderLayout());

        jLabel2.setBackground(new java.awt.Color(51, 51, 255));
        jLabel2.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/messenger.png"))); // NOI18N
        jLabel2.setText("Proyecto Chat");
        panelNorte.add(jLabel2, java.awt.BorderLayout.CENTER);

        bienvenida.setBackground(new java.awt.Color(51, 51, 255));
        bienvenida.setFont(new java.awt.Font("Gabriola", 0, 24)); // NOI18N
        bienvenida.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        bienvenida.setText(" ");
        panelNorte.add(bienvenida, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(panelNorte, java.awt.BorderLayout.NORTH);

        panelSur.setBackground(new java.awt.Color(51, 51, 255));
        panelSur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 255), 5));
        panelSur.setLayout(new java.awt.BorderLayout(5, 0));

        msj.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        msj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                msjActionPerformed(evt);
            }
        });
        panelSur.add(msj, java.awt.BorderLayout.CENTER);

        enviar.setBackground(new java.awt.Color(153, 153, 153));
        enviar.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        enviar.setForeground(new java.awt.Color(51, 51, 255));
        enviar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/messenger+social+telegram+icon-1320194696007326491.png"))); // NOI18N
        enviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enviarActionPerformed(evt);
            }
        });
        panelSur.add(enviar, java.awt.BorderLayout.LINE_END);

        jLabel1.setBackground(new java.awt.Color(51, 51, 255));
        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setText("Escribe un mensaje ...");
        panelSur.add(jLabel1, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(panelSur, java.awt.BorderLayout.PAGE_END);

        jScrollPane4.setBackground(new java.awt.Color(51, 51, 255));
        jScrollPane4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 255), 5));
        jScrollPane4.setForeground(new java.awt.Color(51, 51, 255));

        chat.setColumns(20);
        chat.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        chat.setRows(5);
        jScrollPane4.setViewportView(chat);

        getContentPane().add(jScrollPane4, java.awt.BorderLayout.CENTER);

        jScrollPane2.setBackground(new java.awt.Color(51, 51, 255));
        jScrollPane2.setForeground(new java.awt.Color(255, 255, 255));

        listaConectados.setBackground(new java.awt.Color(51, 102, 255));
        listaConectados.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Conectados", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Gabriola", 0, 24))); // NOI18N
        listaConectados.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jScrollPane2.setViewportView(listaConectados);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(jPanel3, java.awt.BorderLayout.LINE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void msjActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_msjActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_msjActionPerformed

    private void enviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enviarActionPerformed
        // TODO add your handling code here:
        try {
            salida = new DataOutputStream(cliente.getOutputStream());
            salida.writeUTF(nombre+ " : " + this.msj.getText().toString()); //Obtiene el contenido del textField
            salida.flush();
            this.msj.setText("");
        }
        catch(IOException e){

        }
        
    }//GEN-LAST:event_enviarActionPerformed
    
    public void actualizarLista(DefaultListModel modelo) { //actuliza la lista de Contactos
        this.listaConectados.setModel(modelo);
    }   
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PanelChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PanelChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PanelChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PanelChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        
                new PanelChat().setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bienvenida;
    private javax.swing.JTextArea chat;
    private javax.swing.JButton enviar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JList<String> listaConectados;
    private javax.swing.JTextField msj;
    private javax.swing.JPanel panelNorte;
    private javax.swing.JPanel panelSur;
    // End of variables declaration//GEN-END:variables
}
